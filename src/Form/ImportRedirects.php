<?php

namespace Drupal\redirect_mass_create\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\redirect\Entity\Redirect;
use Drupal\Core\Url;

/**
 * Class ImportRedirects.
 */
class ImportRedirects extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'import_redirects';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['import'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Import'),
      '#description' => $this->t('Comma separated list of redirects EG: from,to,code<br> No leading slash "/" path only.'),
      '#required' => true,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array&$form, FormStateInterface $form_state)
  {
    if ($redirects = $this->parse_csv($form_state)) {
      $default_code = $config = \Drupal::config('redirect.settings')->get('default_status_code');
      foreach ($redirects as $redirect) {
        // Set log message for each existing redirect.
        if ($this->check_url($redirect['to']) && $this->validate_code($redirect['code']) && !$exists = $this->existent_redirects($redirect)) {
          $to = $this->clear_url($redirect['to']);
          $to = UrlHelper::isExternal($to) ? $to : 'entity:' . Url::fromUserInput('/' . $to)->getInternalPath();

          // Create redirects.
          Redirect::create(
            [
              'redirect_source' => $redirect['from'],
              'redirect_redirect' => $to,
              'language' => 'und',
              'status_code' => $redirect['code'] ? $redirect['code'] : $default_code,
            ]
          )->save();
        }
      }
      // exit();
    }

  }

  /**
   * Parse CSV string.
   */
  private function parse_csv($form_state)
  {
    if ($import = $form_state->getValue('import')) {
      $lines = explode("\n", $import);
      foreach ($lines as &$val) {
        $arr = explode(',', $val);
        $val = ['from' => $arr[0], 'to' => $arr[1], 'code' => isset($arr[2]) ? $arr[2] : ''];
      }

      return $lines;
    }
  }
  private function clear_url($url)
  {
    return str_replace(array('.', ' ', "\n", "\t", "\r"), '', $url);
  }

  /**
   * Check for if url is valid.
   */
  private function check_url($url)
  {
    // Fix url unwanted characters.
    $url = $this->clear_url($url);

    if (UrlHelper::isValid($url)) {
      return true;
    } else {
      \Drupal::logger('redirect_mass_create')->warning(t('Invalid Destination URL @to_url.', ['@to_url' => $url]));

      return false;
    }
  }

  /**
   * Check for existent url redirect.
   */
  private function existent_redirects($redirect)
  {
    $repository = \Drupal::service('redirect.repository');
    $from = $redirect['from'];
    if ($redirect_exists = $repository->findBySourcePath($from)) {
      \Drupal::logger('redirect_mass_create')->warning(t('Redirect exists for @source_url. Not updated.', ['@source_url' => $from]));

      return true;
    }

    return false;
  }

  private function validate_code($code)
  {
    if ($code == '' || in_array(intval($code), range(300, 307))) {
      return true;
    } else {
      \Drupal::logger('redirect_mass_create')->warning(t('Invalid redirect code @code. Redirect not created.', ['@code' => $code]));

      return false;
    }
  }

}

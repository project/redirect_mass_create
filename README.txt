Redirect Mass Create

Installation
============

- Enable Redirect Mass Create module in the Drupal administration.


Usage
============

- Go to '/admin/config/redirect_mass_create' and paste a comma separated list of redirects eg:

my-path-1,http://johndoe.com
my-path-2,http://johndoe2.com,300
my-path-3,http://johndoe3.com, 305
